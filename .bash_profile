### init ###
eval "$(rbenv init -)"
source $HOME/.ps1_config # load PS1
source $HOME/.profile    # load env variables for projects

### enviroment
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad
export EDITOR=vim
export PATH="$HOME/.cargo/bin:$PATH"

### alias
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME' # git for managing dotfiles
alias ls='ls -GFh'
alias tree='tree -C'
alias alacritty='open -n -a "Alacritty.app"' # because OSX is stupid and this is the only way to get a new alacritty instance

### auto completion
if [ -f $(brew --prefix)/etc/bash_completion ]; then
 . $(brew --prefix)/etc/bash_completion
fi

if [ -f `brew --prefix`/etc/bash_completion.d/git-completion.bash ]; then
  . `brew --prefix`/etc/bash_completion.d/git-completion.bash
fi

### history config
HISTCONTROL=ignoreboth
# append to the history file, don't overwrite it
shopt -s histappend

### docker helpers

# get the logs from a failed container
# use docker service ls to get service id
# then use docker service ps service_id
# then pass this an id from that
failedContainerLog(){
    docker logs $(docker inspect --format "{{.Status.ContainerStatus.ContainerID}}" $1)
}

# make formatted versions of docker commands so they fit in a vertical terminal
docker() {
    if [[ $1 == "ps" ]]; then # use 'docker container ls' for default
        command docker ps --format 'table {{.ID}}\t{{.Image}}\t{{.Ports}}'
    elif [[ $1 == "sls" ]]; then
        command docker service ls --format 'table {{.ID}}\t{{.Name}}\t{{.Image}}\t{{.Replicas}}'
    elif [[ $1 == "sps" ]]; then
        command docker service ps $2 --no-trunc --format 'table {{.ID}}\t{{.Name}}\t{{.CurrentState}}\t{{.Error}}'
    else
        command docker "$@"
    fi
}

enter_container(){
    container=$(docker ps | grep "$1" | awk '{print $1;}')
    echo "attempting to enter container $container"
    docker exec -it $container /bin/bash
}

create_test_secret(){
    echo "test_secret" | docker secret create $1 -
}

create_secret(){
    echo "$1" | docker secret create $2 -
}

### great new terminal sessions
# echo 'hi again' | figlet -f doh | lolcat
